#!/bin/bash

for context in $(shyaml keys <<< "$KUBERNETES_CREDENTIALS"); do
    server=$(shyaml get-value "$context.server" <<< "$KUBERNETES_CREDENTIALS")
    namespace=$(shyaml get-value "$context.namespace" <<< "$KUBERNETES_CREDENTIALS")
    token_name=$(shyaml get-value "$context.token" <<< "$KUBERNETES_CREDENTIALS")
    token=${!token_name}
    kubectl config set-credentials "$context" --token "$token"
    kubectl config set-cluster "$context" --server "$server"
    kubectl config set-context "$context" --cluster="$context" --user="$context" --namespace "$namespace"
    oc --context "$context" whoami
done

kubectl config use-context ocp4_prod
