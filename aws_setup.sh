# not executable as this file should be sourced for the export/unset to have any effect

set -euo pipefail

# without a credentials file, use the deployment service account
if ! [ -f ~/.aws/credentials ]; then
    export AWS_ACCESS_KEY_ID=$DEPLOYMENT_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$DEPLOYMENT_AWS_SECRET_ACCESS_KEY
    unset AWS_PROFILE
fi
